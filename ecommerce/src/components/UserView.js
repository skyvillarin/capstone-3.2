import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';
import SearchByPrice from './SearchByPrice';


export default function UserView({productsData}) {
    /*return(
        <>
            <ProductSearch/>
            <SearchByPrice/>
            { productsData.length && (
                productsData.map(product => {
                    //only render the active products since the route used is /all from Products.js page
                    if(product.isActive) {
                        return <ProductCard productProp={product} key={product._id}/>
                    }
                })
            )}
        </>
        )
}
*/
    const [products, setProducts] = useState([])

    useEffect(() => {
        const productsArr = productsData.map(product => {

            //only render the active products since the route used is /all from Product.js page
            if(product.isActive === true) {
                return (
                    <ProductCard productProp={product} key={product._id}/>
                    )
            } else {
                return null;
            }
        })

        //set the products state to the result of our map function, to bring our returned products component outside of the scope of our useEffect where our return statement below can see.
        setProducts(productsArr)

    }, [productsData])

    return(
        <>
            <ProductSearch/>
            <SearchByPrice/>
            { products }
        </>
        )
}