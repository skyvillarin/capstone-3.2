import { useState } from 'react';
import { Card, Button, Form } from "react-bootstrap";
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import jwt_decode from 'jwt-decode'; 

export default function ProductCard({ productProp }) {
  const { _id, name, description, price } = productProp;
  const [quantity, setQuantity] = useState(1);

  // Function to decode JWT token and get userId
  const getUserIdFromToken = (token) => {
    try {
      const decodedToken = jwt_decode(token);
      return decodedToken.id; 
    } catch (error) {
      console.error('Error decoding JWT token:', error);
      return null;
    }
  };

  const handleCheckout = async () => {
  try {
    const userToken = localStorage.getItem('token');
    if (!userToken) {
      console.error('User is not authenticated');
      Swal.fire({
        icon: 'error',
        title: 'Authentication Error',
        text: 'Please log in with a non-admin account to checkout products.',
      });
      return;
    }

    // Decode the JWT token
    const decodedToken = jwt_decode(userToken);

    // Check if the user is an admin
    if (decodedToken.isAdmin) {
      Swal.fire({
        icon: 'error',
        title: 'Admin Checkout Forbidden',
        text: 'Admins are not allowed to checkout products. Please log in with a non-admin account.',
      });
      return;
    }

    // Get the user ID from the JWT token
    const userId = decodedToken.id;

    // Send a request to the backend to create an order
    const response = await fetch('https://cpstn2-ecommerceapi-villarin.onrender.com/users/checkout', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${userToken}`,
      },
      body: JSON.stringify({
        userId: userId,
        productName: name,
        quantity: quantity,
      }),
    });

    if (!response.ok) {
      throw new Error(`Failed to create the order. Status: ${response.status}`);
    }

    const data = await response.json();

    Swal.fire({
      icon: 'success',
      title: 'Success',
      text: 'Product successfully checked out!',
    });

    console.log(data);
  } catch (error) {
    console.error('Error during checkout:', error);
  }
};

  return (
    <Card className="mt-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Form.Group className="mb-3">
          <Form.Label>Quantity:</Form.Label>
          <Form.Control
            type="number"
            min="1"
            value={quantity}
            onChange={(e) => setQuantity(parseInt(e.target.value))}
            style={{ width: '70px' }}
          />
        </Form.Group>
        <Button variant="primary" onClick={handleCheckout}>
          Checkout
        </Button>
      </Card.Body>
    </Card>
  );
}

// Check if the ProductCard component is getting the correct prop types
ProductCard.propTypes = {
  productProp: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
