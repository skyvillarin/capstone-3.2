import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProducts({product, isActive, fetchData}) {

    const archiveToggle = (productId) => {
        fetch(`https://cpstn2-ecommerceapi-villarin.onrender.com/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            if (data.success) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product successfully delisted'
                })
                fetchData();
            } else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error',
                    text: 'Please Try again'
                })
                fetchData();
            }
        })
    }


        const activateToggle = (productId) => {
        fetch(`https://cpstn2-ecommerceapi-villarin.onrender.com/products/${productId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.success) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product successfully enlisted.'
                })
                fetchData();
            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    }
 

    return(
        <>
            {isActive ?

                <Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

                :

                <Button variant="success" size="sm" onClick={() => activateToggle(product)}>Activate</Button>

            }
        </>

        )
}