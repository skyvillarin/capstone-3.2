import { useState } from 'react';

export default function EditProfile({ user, onUpdate }) {
  const [email, setEmail] = useState(user.firstName);


  const handleUpdate = () => {
    // Create a payload with the updated user data
    const updatedUser = {
      email,
    };

    // Send a PUT request to update the user's profile
    fetch('https://cpstn2-ecommerceapi-villarin.onrender.com/users/profile', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(updatedUser),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        console.log(data);
        onUpdate(data); // Pass the updated user data to the parent component
      })
      .catch((error) => {
        console.error('Error updating user profile:', error);
      });
  };

  return (
    <div className="container mt-5" >
      <h3>Change Registered Email Address</h3>
      <form>
      <div className="mb-3">
        <label className="form-label">New Email:</label>
        <input
          type="text"
          className="form-control"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <button className='btn btn-primary mt-3' onClick={handleUpdate}>Update Email</button>
      </form>
    </div>
  );
}
