import { useState, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'

export default function AppNavbar() {

		const { user } = useContext(UserContext);
		//const [user,setUser] = useState(localStorage.getItem("token"))
		console.log(user);//check if we receive the token


	return (
		<Navbar bg="light" expand="lg">
			<Container fluid>
				
				<Navbar.Brand as={Link} to="/">
					Beauté Rendezvous
				</Navbar.Brand>

				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id='basic-navbar-nav'>
					<Nav className="ms-auto">
					   <Nav.Link as={Link} to="/">Home</Nav.Link>
					   <Nav.Link as={Link} to="/products/">Products</Nav.Link>
						   {(user.id !== null) ? 

						           user.isAdmin 
						           ?
						           <>
						               <Nav.Link as={Link} to="/addProduct">Add Product</Nav.Link>
						               <Nav.Link as={Link} to="/allOrders">All Orders</Nav.Link>
						               <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						           </>
						           :
						           <>
						               <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
						               <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						           </>
						       		: 
						           <>
						               <Nav.Link as={Link} to="/login">Login</Nav.Link>
						               <Nav.Link as={Link} to="/register">Register</Nav.Link>
						           </>
						   }
					   </Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}