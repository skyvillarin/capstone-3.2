import React, { useState } from 'react';

const SearchByPrice = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [products, setProducts] = useState([]);

  const handleMinPriceChange = (e) => {
    setMinPrice(e.target.value);
  };

  const handleMaxPriceChange = (e) => {
    setMaxPrice(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ minPrice, maxPrice }),
    };

    fetch('https://cpstn2-ecommerceapi-villarin.onrender.com/products/searchByPrice', requestOptions)
      .then((response) => response.json())
      .then((data) => {
        // Check if data.products is defined and not empty before setting state
        if (data.products && data.products.length > 0) {
          setProducts(data.products);
        } else {
          // Handle case where no products match the search criteria
          setProducts([]);
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  return (
    <div className="container">
      <h2>Search Products by Price Range</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="minPrice" className="form-label">
            Min Price:
          </label>
          <input
            type="number"
            className="form-control"
            id="minPrice"
            value={minPrice}
            onChange={handleMinPriceChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="maxPrice" className="form-label">
            Max Price:
          </label>
          <input
            type="number"
            className="form-control"
            id="maxPrice"
            value={maxPrice}
            onChange={handleMaxPriceChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Search
        </button>
      </form>
      <h3 className="mt-3">Search Results:</h3>
      <ul>
        {products.map((product) => (
          <li key={product.id}>{product.name}</li>
        ))}
      </ul>
      {products.length === 0 && <p>No products found.</p>}
    </div>
  );
};

export default SearchByPrice;
