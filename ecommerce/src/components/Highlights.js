import { Row, Col, Card } from 'react-bootstrap'


export default function Highlights(){
	
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Revitalize Your Skin's Natural Glow</h2>
						</Card.Title>
							<Card.Text>
							Experience the transformative power of our skincare products designed to rejuvenate and enhance your skin's inherent radiance. Our carefully curated collection offers a holistic approach to skincare, addressing a range of concerns including whitening, anti-aging, rejuvenation, and anti-acne solutions.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Unlock Your Best Self with Our Slimming and Detox Supplements</h2>
						</Card.Title>
							<Card.Text>
							We believe that true beauty begins from within. Our supplement brands are designed to support your body's natural processes, promoting a healthier and more vibrant you. With the power of detoxification and slimming combined, you'll not only look better but feel better too.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Discover Beauty, Unveil Confidence</h2>
						</Card.Title>
							<Card.Text>
							We believe that beauty is a canvas waiting to be revealed. Our mission is to empower you to discover your unique beauty and unveil your inner confidence. With our carefully selected cosmetics, we offer more than just makeup – we provide you with the tools to express your individuality, enhance your features, and embrace your true self.
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}