

import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes, Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

import AppNavbar from './components/AppNavbar'
import AddProduct from './components/AddProduct'
import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
//import ProductView from './pages/ProductView';
import AdminView from './components/AdminView'
import Error from './pages/Error';
import AllOrders from './pages/AllOrders';




import './App.css';
import { UserProvider } from './UserContext';

function App() {

  /*const apiRequestHeaders = () => ({
    Authorization: `Bearer ${localStorage.getItem('token')}`,
  });

  // fetch() function wrapper helper to avoid redundancy code block
  // with authorization token included header
  // and response encoded to JSON format
  const apiFetch = (uri) => {
    return fetch(`http://localhost:4000${uri}`,{
      headers: apiRequestHeaders(),
    }).then(res => res.json());
  };*/

  const [user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    //console.log(user);
    //console.log(localStorage);
    //apiFetch(`/users/details`)
    fetch(`https://cpstn2-ecommerceapi-villarin.onrender.com/users/details`,{
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      console.log("App.js")
      // Set the user state values with the user details upon successful login.
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [user])

  const showAlertForNonAdmin = () => {
    window.alert('Only admins can view the All Orders page.');
  };

  return (
    // React Fragments <></>
    <UserProvider value={{ user, setUser, unsetUser, /*apiFetch*/}}>
    <Router>

      <Container fluid>

        <AppNavbar />

        <Routes>

            <Route path="/" element={<Home/>} />
            <Route path="/profile" element={<Profile/>} />
            <Route path="/products/" element={<Products/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path ="/addProduct" element={<AddProduct/>}/>
            {user.isAdmin ? (
              <Route path="/allOrders" element={<AllOrders />} />
            ) : (
              <Route path="/allOrders" element={showAlertForNonAdmin} />
            )}
            <Route path="*" element={<Error/>} />
            <Route path="/products" element={
                user.isAdmin ? (
                  <AdminView isAdmin={user.isAdmin} />
                ) : (
                  <Navigate to="/" />
                )
              }
            />

        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
