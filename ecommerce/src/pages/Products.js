import {useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';


export default function Products() {

	const { user } = useContext(UserContext);

	// State that will be used to store products retrieved from the database.
	const [products, setProducts] = useState([]);


	// Create a function to fetch all products
	const fetchData = () => {
		fetch(`https://cpstn2-ecommerceapi-villarin.onrender.com/products/all`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			setProducts(data);
			//sets the product state
		});
	}

	// Retrieves the products from the database upon initial render of the "Products" component
	useEffect(() =>{

		fetchData();

	}, []);


	return(
		<>
		    {
		    	(user.isAdmin)
		    		?
		    		<AdminView productsData={products} fetchData={fetchData}/>
		    		:
		    		<UserView productsData={products} />
			}

		</>
	)

};
