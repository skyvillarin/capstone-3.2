import React, { useEffect, useState } from 'react';
import { Table } from "react-bootstrap";

function AllOrders() {
  // State to store the fetched orders data
  const [orders, setOrders] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  // Function to format a number as Philippine Peso currency
  const formatAsPhilippinePeso = (amount) => {
    return new Intl.NumberFormat('en-PH', {
      style: 'currency',
      currency: 'PHP',
    }).format(amount);
  };

  // Fetch orders data from the API
  useEffect(() => {
    fetch('https://cpstn2-ecommerceapi-villarin.onrender.com/orders/all', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrders(data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
        setIsLoading(false);
      });
  }, []); 

  return (
    <div>
      <h1 className="text-center my-4">All Orders</h1>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <Table striped bordered>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>User ID</th>
              <th>Products Ordered</th>
              <th>Quantity</th>
              <th>Total Amount (PHP)</th>
              <th>Purchased On Date</th>
            </tr>
          </thead>
          <tbody>
            {orders.map((order) => (
              <tr key={order._id}>
                <td>{order._id}</td>
                <td>{order.userId}</td>
                <td>
                  {order.products.map((product, index) => (
                    <div key={index}>{product.productName}</div>
                  ))}
                </td>
                <td>
                  {order.products.map((product, index) => (
                    <div key={index}>Qty: {product.quantity}</div>
                  ))}
                </td>
                <td>{formatAsPhilippinePeso(order.totalAmount)}</td>
                <td>{new Date(order.purchasedOn).toLocaleDateString()}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </div>
  );
}

export default AllOrders;
