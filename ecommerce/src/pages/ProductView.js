import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user } = useContext(UserContext);

	// an object with methods to redirect the user
	const navigate = useNavigate();

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const { productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const checkout = (productId) => {
		fetch(`https://cpstn2-ecommerceapi-villarin.onrender.com/users/checkout`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data.message = 'Ordered Successfully.'){
				Swal.fire({
					title: "Successfully ordered",
					icon: 'success',
					text: "You have successfully ordered this item."
				})
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId);

		fetch(`https://cpstn2-ecommerceapi-villarin.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description)
			setPrice(data.price);
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						{ user.id !== null ?
							<Button variant="primary" block onClick={() => checkout(productId)}>Order</Button>
							:
							<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
						}
												
					</Card.Body>
				</Col>
			</Row>
		</Container>
	)
}