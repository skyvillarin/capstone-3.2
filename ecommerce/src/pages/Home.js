import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';

export default function Home() {
  const data = {
    title: "Beauté Rendezvous",
    content: "Beauty and Wellness Hub",
    destination: "/products",
    label: "Order Now!"
  };

  const bannerStyle = {
    marginTop: "10px",
  };

  const imageContainerStyle = {
  	marginTop: "10px",
    marginBottom: "10px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  };

  const imageStyle = {
    maxWidth: "300px", 
    maxHeight: "300px",
    width: "auto",
    height: "auto"
  };

  return (
    <>
      <div style={imageContainerStyle}>
        <img
          src="/images/BRlogo.png"
          alt="BRlogo"
          style={imageStyle}
        />
      </div>
      <Banner data={data} style={bannerStyle} />
      <Highlights />
      <FeaturedProducts />
    </>
  );
}
