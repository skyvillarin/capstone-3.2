import { useContext, useEffect, useState} from 'react';
import { Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useNavigate, Navigate } from 'react-router-dom';
import ResetPassword from '../components/ResetPassword';
import EditProfile from '../components/EditProfile'


export default function Profile(){

    const {user} = useContext(UserContext);

    const[ details, setDetails ] = useState({})

    const handleProfileUpdate = (updatedUser) => {
    setDetails(updatedUser);
  };

    useEffect(()=>{
        fetch(`https://cpstn2-ecommerceapi-villarin.onrender.com/users/details`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data)

            if(typeof data.id !== undefined){

                setDetails(data);
            }
        })
        .catch((error) => {
        console.error('Error fetching user details:', error);
      });
    },[])


    return (
        (details.id === null) ?
            <Navigate to="/products" />
            :
        <>
            <Row>
                <Col className="p-5 bg-primary text-white">
                    <h1 className="my-5 ">Profile</h1>
                    <hr />
                    <h2>Contacts</h2>
                    <ul>
                        <li>Email: {`${details.email}`}</li>
                    </ul>
                </Col>
            </Row>
            <Row className='pt-4 mt-4'>
                <Col>
                    <ResetPassword/>
                    <EditProfile user={details} onUpdate={handleProfileUpdate} />
                </Col>
            </Row>
        </>

    )

}