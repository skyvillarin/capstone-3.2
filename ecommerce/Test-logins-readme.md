## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***

- Regular User:
     - email: pennywise@mail.com
     - pwd: pennywise123
- Regular User:
    - email: leatherface@mail.com
    - pwd: leatherface123

- Admin User:
    - email: jigsaw@mail.com
    - pwd: jigsaw123
- Admin User:
    - email: nosferatu@mail.com
    - pwd: nosferatu123